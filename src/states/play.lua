-- chikun :: 2014
-- Template state


-- Temporary state, removed at end of script
local newState = { }
local player = { }
local collisions = { }
view = {
    x = 0,
    y = 0,
    w = 854,
    h = 480
    }

-- On state create
function newState:create()

    for key, layer in ipairs(map.current.layers) do

        if layer.name == 'collisions' then
            collisions = layer.objects
        end

        if layer.name == 'important' then

            for key, object in ipairs(layer.objects) do

                if object.name == 'spawn' then

                    player = object
                    player.xMove = 0
                    player.yMove = 0

                end

            end

        end

    end

end


-- On state update
function newState:update(dt)

    -- Jumping
    local jumpPad = {
        x = player.x,
        y = player.y + player.h + 1,
        w = player.w,
        h = 1
    }
    if input.check('action') and player.yMove >= 0 then
        if math.overlapTable(collisions, jumpPad) then
            player.yMove = -275
        end
    end

    -- Gravity
    player.yMove = player.yMove + dt * 512

    -- Gravity limiter
    if player.yMove >= 2000 then
        player.yMove = 2000
    end

    -- Y plane Movement
    player.y = player.y + player.yMove * dt

    -- Y plane collision
    while math.overlapTable(collisions, player) do

        -- Set yMove to a smaller integer
        if player.yMove > 0 then
            player.yMove = 1
        elseif player.yMove <= 0 then
            player.yMove = -1
        end

        -- Move player out of wall slowly
        player.y = player.y - player.yMove * dt
        if not math.overlapTable(collisions, player) then
            player.yMove = 0
        end
    end

    -- Check if moving left or right
    if input.check('left') or input.check('right') then

        -- Reset xMove
        player.xMove = 0

        -- Fine tune if moving left or right
        if input.check('left') then
            player.xMove = -1
        end
        if input.check('right') then
            player.xMove = player.xMove + 1
        end
    end

    -- Move player on X plane
    player.x = player.x + player.xMove * dt * 128

    -- X plane collision
    while math.overlapTable(collisions, player) do
        player.x = player.x - player.xMove * dt
    end

    -- Reset xMove
    player.xMove = 0

    -- View movement
    view.x = player.x - (view.w - player.w) / 2
    view.y = player.y - (view.h - player.h) / 2
    view.x = math.clamp(0, view.x, map.current.w * map.current.tileW - view.w)
    view.y = math.clamp(0, view.y, map.current.h * map.current.tileH - view.h)

end


-- On state draw
function newState:draw()

    -- Move screen to focus on player
    g.translate(-view.x, -view.y)

    g.setColor(255, 255, 255)

    -- Draw the map
    map.draw()

    -- Draw a placeholeder for player
    g.rectangle('fill', player.x, player.y, player.w, player.h)

    -- Stop translating for GUI
    g.origin()

end


-- On state kill
function newState:kill()

end


-- Transfer data to state loading script
return newState
