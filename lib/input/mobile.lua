-- chikun :: 2014
-- Handles input for Android devices

-- The table that houses input functions
input = { }

-- Actually checks if a button is pressed
function input.check(inputType)

    -- Our return value
    local keyPressed = false

    if love.touch.getTouchCount() > 0 and inputType ~= nil then
        for index = 1, love.touch.getTouchCount() do
            local id, x, y, pressure = love.touch.getTouch(index)
            if  keys[inputType][1] < x and
                x < keys[inputType][2] and
                keys[inputType][3] < y and
                y < keys[inputType][4] then
                    keyPressed = true
            end
        end
    end

    return keyPressed

end
